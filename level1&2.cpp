#include<iostream>
#include<string>
#include<sstream>
#include<cstdlib>
using namespace std;


float To_number (string x){
	float v;
	v=atof(x.c_str());
	return v;
}

int To_number_int (string x){
	int v;
	v=atoi(x.c_str());
	return v;
}


string To_string (float d){
	string str;
	ostringstream ss;
	ss<<d;
	str=ss.str();
	return str;
}

string Add (string w,string y){
	float z= To_number(w) + To_number(y);
	string result=To_string(z);
	return result;
}

int main(){
	
	int n;
	cin>>n;
	string A[100];
	for(int i=0;i<n;i++){
		cin>>A[i];
	}
	string order,temp;
	cin.ignore();
	getline(cin,order);
    
    float MatrixOfCoefficients[100][100];
    
    for(int i=0;i<100;i++)
        for(int j=0; j<100; j++)
            MatrixOfCoefficients[i][j]=0;
    
    bool invertFlag;
    
	float coeff=0;
	string var;
	int var_int=0;
	for(int i=0;i<n;i++){// loops on equations
	 string x=A[i];
	 invertFlag=false;
	 //cout << "Equation Number " << i << endl;
	 
	 for(int j=0;j<x.length();j++)//loops on digits in one equation
	 {
	     coeff = 0;
	     var = "0";
	     var_int=0;
		 //cout << "x[j] = " << x[j] << endl;
		 if(48<=x[j]&&x[j]<=57)
		{
		int c=j+1;
		while((48<=x[c]&&x[c]<=57)||x[c]=='.')//should be ASCII
		{
			c++;
		}
		coeff = To_number(x.substr(j,c-j));
		//cout << "Coeff= " << coeff << endl;
		 if(x[c]=='+'||x[c]=='-'||x[c]=='='||c==x.length()){var="0";j=c-1;}//this is a constant
		 else if(x[c]=='x'){
		    //cout << "c=" <<c << endl;
		    int z=c+1;
		 while(x[z]!='+'&&x[z]!='-'&&x[z]!='='&&z!=x.length())
		 {
		     z++;
		 }
		 var=x.substr(c+1,z-c-1);
		 //cout << "var = " << var << endl;
		 j=z-1;
		 }
		 var_int = To_number_int(var);
		 //cout << "Variable= " << var_int << endl;
		 goto end;
	    }
	    
	    
	    
	    
		 else if(x[j]=='x'){
		    int m=j+1;
		 while(x[m]!='+'&&x[m]!='-'&&x[m]!='='&&m!=x.length())
		 {
		     m++;
		 }
		 coeff=1;
		 //cout << "Coeff= " << coeff << endl;
		 var=x.substr(j+1,m-j-1);
		 //cout << var << endl;
		 var_int = To_number_int(var);
		 //cout << "Variable= " << var_int << endl;
		 j=m-1;
		 goto end;
		 }

        
        
        
        
		 else if(x[j]=='-'&&(48<=x[j+1]&&x[j+1]<=57)){
			 int q=j+1;
			 
			 while((48<=x[q]&&x[q]<=57)||x[q]=='.')
			 {
			     q++; 
			 }
			 
			 coeff=To_number(x.substr(j,q-j));
			 //coeff *= -1;
			 //cout << "Coeff= " << coeff << endl;
			 
			 if(x[q]=='-'||x[q]=='+'||x[q]=='='||q==x.length())
			 {
			     var="0";
			     var_int = To_number_int(var);
			     //cout << "Variable= " << var_int << endl;
			     j=q-1;
			     goto end;
			 }
			 
			 else if(x[q]=='x')
			 {
			     int m=q+1;
        		 while(x[m]!='+'&&x[m]!='-'&&x[m]!='='&&m!=x.length())
        		 {
        		     m++;
        		 }
        		 var=x.substr(q+1,m-q);
        		 //cout << var << endl;
        		 var_int = To_number_int(var);
        		 //cout << "Variable= " << var_int << endl;
        		 j = m-1;
        		 goto end;
			 }
		 }
		 
		 
		 
		 
		 else if (x[j]=='-'&& x[j+1]=='x'){
		 coeff=-1;
		 int qq=j+2;
		 if(x[qq]!='+'&&x[qq]!='-'&&x[qq]!='='&&qq!=x.length()){
		    qq++;
		 }
		 //cout << "Coeff= " << coeff << endl;
		 var=x.substr(j+2,qq-j-1);
		 //cout << var << endl;
		 var_int = To_number_int(var);
         //cout << "Variable= " << var_int << endl;
         j = qq;
         goto end;
		 }
		 
		 
		 else if(x[j]=='+')
		 {
		     goto final;
		 }

		 
        
        else if(x[j]=='=')
        {
            invertFlag = true;
            //cout << "flagInverted" << endl;
            goto final;
        }
        
        end:
        
        if(invertFlag==true)
	    {MatrixOfCoefficients[i][var_int] -= coeff;}
	    else
	    {MatrixOfCoefficients[i][var_int] += coeff;}
	    //cout << MatrixOfCoefficients[i][var_int] << endl;
	    //cout << "---------------------" << endl;
	    
	    final:;
	}
        
	
	}
	////Testing Output Loop
	for(int i=0;i<100;i++)
	{
	    for(int j=0;j<100;j++)
		{
		    cout<<MatrixOfCoefficients[i][j]<<"\t";
	    }
	    cout << endl;
	}
	
	
	if(order.substr(0,8)=="equation"){
	string tt=order.substr(9);
	int st=To_number_int(tt)-1;
	for(int j=1;j<100;j++)
	{
	    if((MatrixOfCoefficients[st][j]!=1) && (MatrixOfCoefficients[st][j])!=-1 && (MatrixOfCoefficients[st][j]!=0)){
	        cout<<MatrixOfCoefficients[st][j]<<"x"<<j;
	   if (MatrixOfCoefficients[st][j+1]>0)
	            cout<<"+";
	    }
		if(MatrixOfCoefficients[st][j]==1){
	      cout<<"x"<<j;
		  if (MatrixOfCoefficients[st][j+1]>0)
	            cout<<"+";
		}
		  if(MatrixOfCoefficients[st][j]==-1){
	        cout<<"-x"<<j;
			if (MatrixOfCoefficients[st][j+1]>0)
	            cout<<"+";
		  }
	    
			if(MatrixOfCoefficients[st][j]==0){
			    if(MatrixOfCoefficients[st][j+1]>0)
			     cout<<"+";
				goto escape;
			}
		escape:;
	}
	cout<<"="<<(MatrixOfCoefficients[st][0])*-1;
	}
	
	
	
if(order.substr(0,6)=="column"){
    string ww=order.substr(8);
    for(int i=0;i<n;i++)
    cout<<MatrixOfCoefficients[i][To_number_int(ww)]<<endl;
}



if(order=="num_vars"){
    int count=0;
    for(int j=1;j<100;j++){
        for(int i=0;i<n;i++){
            if(MatrixOfCoefficients[i][j]!=0)
            {
                count++;
                break;
            }
       }
    
    }
    cout<<count;
}


if(order.substr(0,3)=="add"){
    string b="",c="";
    int i=4;
    while(48<=order[i]&&order[i]<=57){
        b+=order[i];
        i++;
    }
    i++;

    while(48<=order[i]&&order[i]<=57){
        c+=order[i];
        i++;
    }
    
    int bb,cc;
    bb = To_number_int(b);
    cc = To_number_int(c);
    
    //To get the real index of the equation in the Matrix
    bb--;
    cc--;
    
    for(int j=1;j<100;j++)
    {
        if(j==1)
        {
            if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]==0)
                {
                   goto endOfLoop; //to increase j
                }
            else
            {
                if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]==1)
                  cout<< "x" << j;
                
                else if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]>0)
                {
                    cout << MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j];
                    cout << "x" << j;
                }
                
                else if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]==-1)
                     cout<< "-x" << j;
                     
                else if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]<0)
                {
                    cout << MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j];
                    cout << "x" << j;
                }
                
                
            }
        }
        else
        {
            if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]==0)
                {
                   goto endOfLoop; // to increase j
                }
            else
            {
                if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]==1)
                {
                   cout <<"+";
                   cout <<"x"<<j;
                }
                else if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]>0)
                {
                    cout << "+" ;
                    cout << MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j];
                    cout << "x" << j;
                }
                else if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]==-1)
                {
                    cout << "-x" <<j;
                }
                else if(MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j]<0)
                {
                    cout << MatrixOfCoefficients[bb][j]+MatrixOfCoefficients[cc][j];
                    cout << "x" << j;
                }
                
            }
        }
     
    
    
    endOfLoop:; //To increase j only
        
    }
    cout << "=";
    
    if((MatrixOfCoefficients[bb][0]+MatrixOfCoefficients[cc][0])==0)
    {
        cout << "0";
    }
    else
    {
        cout <<(MatrixOfCoefficients[bb][0]+MatrixOfCoefficients[cc][0])*-1;
    }
}  

if(order.substr(0,8)=="subtract"){
    string d="",e="";
    int j=9;
    while(48<=order[j]&&order[j]<=57){
        d+=order[j];
        j++;
    }
    j++;

    while(48<=order[j]&&order[j]<=57){
        e+=order[j];
        j++;
    }
    
    int dd,ee;
    dd = To_number_int(d);
    ee = To_number_int(e);
    
    //To get the real index of the equation in the Matrix
    dd--;
    ee--;
    
    for(int j=1;j<100;j++)
    {
        if(j==1)
        {
            if(MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j]==0)
                {
                   goto finish; //to increase j
                }
            else
            {
                 if(MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j]>0)
                {
                    cout << MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j];
                    cout << "x" << j;
                }
                
                else if(MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j]<0)
                {
                    cout << MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j];
                    cout << "x" << j;
                }
                
                
            }
        }
        else
        {
            if(MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j]==0)
                {
                   goto finish; // to increase j
                }
            else
            {
                
                 if(MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j]>0)
                {
                    cout << "+" ;
                    cout << MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j];
                    cout << "x" << j;
                }
                
                else if(MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j]<0)
                {
                    cout << MatrixOfCoefficients[dd][j]-MatrixOfCoefficients[ee][j];
                    cout << "x" << j;
                }
                
            }
        }
     
    
    
    finish:; //To increase j only
        
    }
    cout << "=";
    
    if((MatrixOfCoefficients[dd][0]-MatrixOfCoefficients[ee][0])==0)
    {
        cout << "0";
    }
    else
    {
        cout <<(MatrixOfCoefficients[dd][0]-MatrixOfCoefficients[ee][0])*-1;
    }
}   




	//cout<<temp;
	//cout << order;
return 0;
	}